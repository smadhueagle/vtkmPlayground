#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/Math.h>
struct Sine: public vtkm::worklet::WorkletMapField {
    typedef void ControlSignature(FieldIn<>, FieldOut<>);
    typedef _2 ExecutionSignature(_1);

    template<typename T>
    VTKM_EXEC_EXPORT
    T operator()(T x) const {
        return vtkm::Sin(x);
    }
};


int main(int argc, char** argv){
    std::vector <vtkm::Float32 > dataBuffer;

    for(vtkm::Float32 i=0; i<=2.0*vtkm::Pi();i+=0.05){
        dataBuffer.push_back(i);
    }
    vtkm::cont::ArrayHandle<vtkm::Float32> inputHandle = vtkm::cont::make_ArrayHandle(dataBuffer);
    vtkm::cont::ArrayHandle<vtkm::Float32> sineResult;

    vtkm::worklet::DispatcherMapField<Sine> dispatcher;
    dispatcher.Invoke(inputHandle, sineResult);
}
